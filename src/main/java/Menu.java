
public enum Menu {
  ONE(1),TWO(2),THREE(3),FOUR(4),SIZE(5),EXIT(0);
  Menu(int id){
    this.id = id;
  }

  public int getId() {
    return id;
  }
  public static Menu getMenu(int id){
    for (int i = 0; i < Menu.values().length; i++) {
      if(Menu.values()[i].getId() == id){
        return Menu.values()[i];
      }
    }
    return null;
  }

  private int id;
}
